#!/usr/bin/python3
# coding: utf8
import sqlite3
import json

fichierBDD="BDD.sqlite"

def initialiserConnexion():
	global connexion
	connexion = sqlite3.connect(fichierBDD) #ouverture de la connexion
	global c
	c = connexion.cursor()

def terminerConnexion():
	connexion.commit() #sauvegarde des changements
	connexion.close() #fermeture de la connexion
	
def creationBDD():
	initialiserConnexion()
	c.execute('''CREATE TABLE prises (MAC text, nom_prise text, enable integer, state integer);''') #création de la table prises
	c.execute('''CREATE TABLE plagesHoraires (MAC text, debut text, fin text, idTimer text);''') #création de la table plagesHoraires
	terminerConnexion()

def getPrises():
	initialiserConnexion()
	contenu = c.execute('SELECT * FROM "prises";')
	contenu = [dict(zip([key[0] for key in c.description], row)) for row in contenu]
	return(json.dumps(contenu)) #renvoi de l'object JSON contenant les prises
	terminerConnexion()

def getPlagesHoraires():
	initialiserConnexion()
	contenu = c.execute('SELECT * FROM "plagesHoraires";')
	contenu = [dict(zip([key[0] for key in c.description], row)) for row in contenu]
	return(json.dumps(contenu)) #renvoi de l'object JSON contenant les prises
	terminerConnexion()

	
def activerPrise(adresseMAC, nomPrise):
	initialiserConnexion()
	c.execute("SELECT * FROM prises WHERE MAC=?", (adresseMAC,))
	ligne = c.fetchone()
	if ligne is None: #si la prise n'est pas encore enregistrée
		protectionInjection = (adresseMAC, nomPrise,)
		c.execute('INSERT INTO prises VALUES (?, ?, 1, 0)', protectionInjection) #création de celle-ci
	else:
		c.execute('UPDATE prises SET enable=1 WHERE MAC=?', (adresseMAC,))
		protectionInjection = (nomPrise, adresseMAC, )
		c.execute('UPDATE prises SET nom_prise=? WHERE MAC=?', protectionInjection)
	terminerConnexion()
	
def desactiverPrise(adresseMAC):
	initialiserConnexion()
	c.execute('SELECT * FROM prises WHERE MAC=?', (adresseMAC,))
	ligne = c.fetchone()
	if ligne is not None:
		c.execute('UPDATE prises SET enable=0 WHERE MAC=?', (adresseMAC,))
	else:
		print ("ERREUR : IMPOSSIBLE DE DESACTIVER UNE PRISE INEXISTANTE")
	terminerConnexion()

def updateEtat(adresseMAC, nouvelEtat):
	initialiserConnexion()
	c.execute('SELECT * FROM prises WHERE MAC=?;', (adresseMAC,))
	ligne = c.fetchone()
	if ligne is not None:
		protectionInjection = (nouvelEtat, adresseMAC,)
		c.execute('UPDATE prises SET state=? WHERE MAC=?;', protectionInjection)
	else:
		print ("ERREUR : IMPOSSIBLE DE CHANGER L'ETAT D'UNE PRISE INEXISTANTE")
	terminerConnexion()
	
def ajouterPlageHoraire(adresseMAC, debut, fin, idTimer):
	initialiserConnexion()
	c.execute('SELECT * FROM plagesHoraires WHERE MAC=? AND debut=?;', (adresseMAC, debut,))
	ligne = c.fetchone()
	if ligne is None:
		c.execute('SELECT * FROM plagesHoraires WHERE MAC=? AND fin=?;', (adresseMAC, fin,))
		ligne = c.fetchone()
		if ligne is None:
			protectionInjection = (adresseMAC, debut, fin, idTimer)
			c.execute('INSERT INTO plagesHoraires VALUES (?, ?, ?, ?)', protectionInjection) #création de celle-ci
	terminerConnexion()

try: 
	fh = open(fichierBDD, 'r')  #si le fichier sqlite n'existe pas
except FileNotFoundError: 
	creationBDD() #création de celui ci