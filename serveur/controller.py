#!/usr/bin/python3
# coding: utf8

import json
import sqliteConnector

def receptionMQTT(topic, payload):
	objetJson = json.loads(payload.decode())
	if topic == 'signal':
		adresseMAC = objetJson['MAC']
		if objetJson['enable'] == 1: #activation d'une prise
			nomPrise = objetJson['nom_prise']
			activerPrise(adresseMAC, nomPrise)
		elif objetJson['enable'] == 0: #desactivation d'un prise
			desactiverPrise(adresseMAC)
	elif topic.split('/')[0] == 'control':
		if objetJson['type'] == 'return':
			adresseMAC = topic.split('/')[1]
			nouvelEtat = objetJson['state']
			updateEtat(adresseMAC, nouvelEtat)
	elif topic == 'ajouterPlageHoraire':
		adresseMAC = objetJson['MAC']
		debut = objetJson['debut']
		fin = objetJson['fin']
		idTimer = objetJson['idTimer']
		ajouterPlageHoraire(adresseMAC, debut, fin, idTimer)	
	else: print("MQTT recu, pas d'action : ", topic, objetJson)

def activerPrise(adresseMAC, nomPrise):
	print("activerPrise(" + adresseMAC + ", " + nomPrise + ")")
	sqliteConnector.activerPrise(adresseMAC, nomPrise)

def desactiverPrise(adresseMAC):
	print("desactiverPrise(" + adresseMAC + ")")
	sqliteConnector.desactiverPrise(adresseMAC)

def updateEtat(adresseMAC, nouvelEtat):
	print("updateEtat("+ adresseMAC + ", " + str(nouvelEtat) + ")")
	sqliteConnector.updateEtat(adresseMAC, str(nouvelEtat))
	
def ajouterPlageHoraire(adresseMAC, debut, fin, idTimer):
	print("ajouterPlageHoraire(" + adresseMAC + ", " + debut + ", " + fin + ", " + idTimer + ")")
	sqliteConnector.ajouterPlageHoraire(adresseMAC, debut, fin, idTimer)
