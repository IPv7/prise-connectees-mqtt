#!/usr/bin/python3
# coding: utf8

from http.server import BaseHTTPRequestHandler, HTTPServer
import ssl
import json
import sqliteConnector
import hashlib

# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		try:
			#Check the file extension required and
			#set the right mime type
			hardFile = None
			if self.path.endswith(".png"):
				mimetype = 'image/png'
				accessType = 'rb'
				hardFile = True
			if self.path.endswith(".ico"):
				mimetype = 'image/x-icon'
				accessType = 'rb'
				hardFile = True
			elif self.path.endswith(".js"):
				mimetype = 'application/javascript'
				accessType = 'r'
				hardFile = True
			elif self.path.endswith(".css"):
				mimetype = 'text/css'
				accessType = 'r'
				hardFile = True
			elif self.path.endswith(".html") or self.path.endswith("/"):
				mimetype = 'text/html'
				accessType = 'r'
				hardFile = False
			elif self.path.endswith(".json"):
				mimetype = 'application/json'
				accessType = 'r'
				hardFile = False
				

			if hardFile == True:
				#Open the static file requested and send it
				print("PATH : '" + str(self.path[1:]) + "'")
				f = open('./http/' + self.path[1:], accessType)
				contenu = f.read()
				f.close()
			elif hardFile == False:
				if mimetype == "text/html":
					html = open('./http/login.html', accessType)
					contenu = html.read()
					html.close()
				elif mimetype == 'application/json':
					if (str(self.path[1:]) == 'prises.json'):
						contenu = sqliteConnector.getPrises()
					elif (str(self.path[1:]) == 'plages.json'):
						contenu = sqliteConnector.getPlagesHoraires()
					else: hardFile = None
						
			
			if hardFile == None:
				self.send_error(404,'ERREUR 404 : %s' % self.path)
			else :
				if accessType == 'r':
					contenu = contenu.encode()
				self.send_response(200)
				self.send_header('Content-type', mimetype)
				self.end_headers()
				self.wfile.write(contenu)
			return
		except IOError:
			self.send_error(404,'ERREUR 404 : %s' % self.path)


	def do_POST(self):
		 
		datalen = int(self.headers['Content-Length'])
		data = self.rfile.read(datalen)
		postContent = data.decode()
		userString = postContent.rsplit("&")[0]
		passwordString = postContent.rsplit("&")[1]
		if(((hashlib.sha256(userString.encode()).hexdigest()) == "09b9ba424f54511d36df52523d5b39f329eef7e6af3827452bcfc5603a9f85a7") and ((hashlib.sha256(passwordString.encode()).hexdigest()) == "ccf65512b6d8b8b575118e49e1e1aca582e44e6c0feac61e832716b0cfdcf2f1")):
			html = open('./http/controle.html', 'r')
			contenu = html.read()
			html.close()
			contenu = contenu + '</div></body></html>'
		else:
			html = open('./http/login.html', 'r')
			contenu = html.read()
			html.close()
			contenu = contenu + '<div class="alert alert-danger"><strong>CLONG</strong></div></div></body></html>'
		self.send_response(200)
		self.send_header('Content-type', "text/html")
		self.end_headers()
		self.wfile.write(contenu.encode())
		return

def run():
	print('starting server...')
	# Server settings
	# Choose port 8080, for port 80, which is normally used for a http server, you need root access
	server_address = ('', 443)
	global httpd
	httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
	print('running server...')
	httpd.socket = ssl.wrap_socket(httpd.socket, keyfile="./ssl/privkey.pem", certfile="./ssl/cert.pem", server_side=True)
	httpd.serve_forever()

try:
	run()
except KeyboardInterrupt:
	print("\nbye")
	httpd.socket.close()

