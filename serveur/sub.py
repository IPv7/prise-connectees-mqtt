#!/usr/bin/python3
# coding: utf8

import configMQTT

import paho.mqtt.client as mqtt
import controller

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
	if rc != 0:
		print("CLONG")
		exit()
	else: print("Ca a l'air oké ;p")

	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	client.subscribe("#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
	#print(msg.topic, msg.payload)
	try:
		controller.receptionMQTT(msg.topic, msg.payload)
	except Exception as error:
		print(type(error))
		print(error) 

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(configMQTT.username, configMQTT.password)
client.connect(configMQTT.hostname, configMQTT.port, configMQTT.timeout)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()





