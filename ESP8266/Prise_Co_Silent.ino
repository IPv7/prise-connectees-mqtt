#include <PubSubClient.h>
#include <FS.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson 

#define RES_PIN 4  // RECONFIGURATION
#define ONOFF_PIN 5  // 
#define OUTPUT_PIN 12    // PIN DE SORTIE MQTT



// Info MQTT enregistrées dans fichier json
char nom_prise[20] = "PRISE 1 GROUPE 1";
char mqtt_server[30] = "176.130.89.135";             
char mqtt_port[6] = "9004";
char mqtt_user[20] = "admin";
char mqtt_pass[20] = "toto";
byte state = 0;  //état de la prise 0 OFF 1 ON

char mqtt_sign_topic[40] = "signal";                 // Info MQTT "EN DUR"
char mqtt_cont_topic[40] = "control/";
char macAddr[18]="";


WiFiClient espClient;   // WiFiClient : librairie bas niveau de wifiManager, permet d'avoir le bon client WIFI
WiFiManager wifiManager;  //objet de gestion du wifi
PubSubClient client(espClient); // Création du client MQTT

bool shouldSaveWifiConfig = false; // FLAG de sauvegarde conf Wifi

void saveWifiConfig () {
  shouldSaveWifiConfig = true;
}

void startwifiandMQTT () {

  // Ajout des informations suplémentaire dans la page de réglage WifiManager
  WiFiManagerParameter custom_text("<p>Informations MQTT :</p>"); 
  WiFiManagerParameter custom_nom_prise("nom", "nom de la prise", nom_prise, 20);
  WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 30);
  WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
  WiFiManagerParameter custom_mqtt_user("user", "mqtt user", mqtt_user, 20);
  WiFiManagerParameter custom_mqtt_pass("password", "mqtt pass", mqtt_pass, 20);

  wifiManager.addParameter(&custom_text);
  wifiManager.addParameter(&custom_nom_prise);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_user);
  wifiManager.addParameter(&custom_mqtt_pass);

  wifiManager.setSaveConfigCallback(saveWifiConfig); //fonction qui demande la sauvegarde quand la page de réglage est fermée

  // wifiManager.autoConnect : soit connexion sur un wifi sauvegardé (Wifimanager s'occupe de la sauvegarde)
  // ou création d'un Access Point. SSID : CONFIGURATION-PRISE MDP : CONF-ESP8266
  if (!wifiManager.autoConnect("CONFIGURATION-PRISE", "CONF-ESP8266")) {  
    Serial.println("Connection WIFI NOK, verifier les paramètres");
    delay(3000);
    ESP.reset();
  }

  // copie des paramètres récupérés dans leur variables
  strcpy(nom_prise, custom_nom_prise.getValue());
  strcpy(mqtt_server, custom_mqtt_server.getValue());
  strcpy(mqtt_port, custom_mqtt_port.getValue());
  strcpy(mqtt_user, custom_mqtt_user.getValue());
  strcpy(mqtt_pass, custom_mqtt_pass.getValue());

  client.setServer(mqtt_server,atoi(mqtt_port));
  client.setCallback(mqttRead);  
  Serial.println("Reglage Wifi + MQTT ok");
}

void reconnect() {
  
  //création du msg json de connexion MQTT
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonCo = jsonBuffer.createObject();
  jsonCo["MAC"] = macAddr;
  jsonCo["nom_prise"] = nom_prise;
  jsonCo["enable"] = 1;

  char msg[74];
  jsonCo.printTo(msg);

  //création du lastWill MQTT en json    
  JsonObject& jsonLastWill = jsonBuffer.createObject();
  jsonLastWill["MAC"] = macAddr;
  jsonLastWill["enable"] = 0;
  
  char lastWill[74];
  jsonLastWill.printTo(lastWill);
  
  int i =0; //compteur de tentatives de co'
  
  // boucle jusqu'a la connexion
  while (!client.connected()) {
    
    // Connexion du client, avec authentification, lastwill (qos 1). Ferme la session précédente si il y en a une
    if (client.connect(macAddr,mqtt_user,mqtt_pass,mqtt_sign_topic,1,0,lastWill,true)) {
      
      Serial.println("Connexion MQTT OK");

      client.publish(mqtt_sign_topic,msg); //envois de l'annonce, pour création d'un topic de controle côté serveur
      Serial.println("Ecoute sur le topic :");
      Serial.println(mqtt_cont_topic);
      client.subscribe(mqtt_cont_topic,1); //écoute sur le topic de controle
      
    } else {
      Serial.print("connection impossible, code d'erreur =");
      Serial.print(client.state());    //voir https://pubsubclient.knolleary.net/api.html#state pour détails des codes erreur
      Serial.println("Nouvelle tentative dans 5s");
      delay(5000);
    }
     if(i>100){ //si co' impossible : reset des règlages et setUP à nouveaux
       WiFi.disconnect();
       startwifiandMQTT ();
    }
  }
}

void mqttRead(char* topic, byte* payload, unsigned int length) {
  Serial.println("Msg MQTT reçut");
  
  DynamicJsonBuffer jsonBuffer;
  JsonObject& jsonPayl = jsonBuffer.parseObject(payload);
  String type = jsonPayl["type"]; 

  if (type=="order"){ //vérifie si c'est bien un ordre   
    if (!(state==jsonPayl["state"])){ //change l'état si il y a lieux
      state = jsonPayl["state"];
      changeState();      
    }
  }  
}

void changeState() {
  //création de la confirmation json du changement d'état
  DynamicJsonBuffer jsonBuffer; 
  JsonObject& jsonStateRet = jsonBuffer.createObject();
  jsonStateRet["type"] = "return";
  jsonStateRet["state"] = state;
  char msg[28];
  jsonStateRet.printTo(msg);

  //changement d'état et envois de la confirmation
  if (state == 1) {
    Serial.println("Output ON");
    digitalWrite(OUTPUT_PIN, HIGH);   // OUT ON
  }
  if (state == 0){
    Serial.println("Output OFF");
    digitalWrite(OUTPUT_PIN, LOW);  // OUT OFF
  }
  
  client.publish(mqtt_cont_topic,msg);
  
  shouldSaveWifiConfig = true; //demande la sauvegarde

}

void setup() {
  Serial.begin(115200);
  
  Serial.println("~~~~~~~~~~~~~~ PRISE CONNECTEE GROUPE 1 RT22A ~~~~~~~~~~~~~~~~ ");
  Serial.println("~~~~~~~~ Mathias Jacquot-Albrecht / Guillaume Collard ~~~~~~~~ ");
  Serial.println("~~~~~~~~ Kilian Florance / Julien Raul / Loan De Mil ~~~~~~~~~ ");
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~ Code Non Verbeux ~~~~~~~~~~~~~~~~~~~~~~ ");
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
  Serial.println("");
  
  pinMode(RES_PIN, INPUT);
  pinMode(ONOFF_PIN, INPUT);
  pinMode(OUTPUT_PIN, OUTPUT);
  
  WiFi.macAddress().toCharArray(macAddr,18);   // @mac utilisée pour crée un channel de controle par prise
  strcat(mqtt_cont_topic,macAddr);

   //Lecture du fichier de conf'
  if (SPIFFS.begin()) {
    if (SPIFFS.exists("/config.json")) {    // si mémoire existe
      
      File configFile = SPIFFS.open("/config.json", "r"); //ouverture en lecture
      if (configFile) { // si le fichier de configuration existe

        size_t size = configFile.size();  // création d'un buffer de la taille du fichier
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size); //création d'un objet Json et parse du contenus
        DynamicJsonBuffer jsonBuffer;
        JsonObject& jsonConf = jsonBuffer.parseObject(buf.get());  
        
        if (jsonConf.success()) {
          
          strcpy(mqtt_server, jsonConf["mqtt_server"]);  //copie des valeurs
          strcpy(mqtt_port, jsonConf["mqtt_port"]);
          strcpy(mqtt_user, jsonConf["mqtt_user"]);
          strcpy(mqtt_pass, jsonConf["mqtt_pass"]);
          strcpy(nom_prise, jsonConf["nom_prise"]);
          state = jsonConf["state"];

          Serial.println("Lecture de la configuration Ok");
        
        } else {
          Serial.println("Import Json impossible");
        }
        configFile.close();
      }
    }
  } else {
    Serial.println("Montage mémoire impossible");
  }

  if (state == 1) {  // change état selon état sauvegardé
    digitalWrite(OUTPUT_PIN, HIGH);   // OUT ON
  }else{
    digitalWrite(OUTPUT_PIN, LOW);  // OUT OFF
  }
  
  startwifiandMQTT (); // Lancement du wifiManager

}

void loop() {

  if (!client.connected()) {  //si le client MQTT n'est pas connecté : envois vers la connexion 
    reconnect();
  }
  client.loop(); // indique au client de verifier l'arivé de msg MQTT

  if (shouldSaveWifiConfig) { // enregistrement de la configuration
    
    //met la configuration dans un json
    DynamicJsonBuffer jsonBuffer;
    JsonObject& json = jsonBuffer.createObject(); 
    json["mqtt_server"] = mqtt_server;
    json["mqtt_port"] = mqtt_port;
    json["mqtt_user"] = mqtt_user;
    json["mqtt_pass"] = mqtt_pass;
    json["nom_prise"] = nom_prise;
    json["state"] = state;

    //écriture en mémoire
    File configFile = SPIFFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("Impossible d'ouvrir le fichier de configuration pour l'enregistrer");
    }
    
    json.printTo(configFile);
    configFile.close();
    Serial.println("Enregistrement de la configuration ok");
    shouldSaveWifiConfig = false;
  }

  if ( digitalRead(RES_PIN) == HIGH ) { // RESET de la configuration puis reconfiguration
    Serial.println("********** Reset des réglages ******** ");
    //client.disconnect();
    WiFi.disconnect();
    startwifiandMQTT ();
  }

  if ( digitalRead(ONOFF_PIN) == HIGH ) { // Changement d'état local
    delay(500);
    
    if(state== 1){ //change la variable state avant de changer l'état physique
      state = 0;
    }else{
      state = 1;
    }
    changeState();
  }
}
