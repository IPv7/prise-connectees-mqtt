# Prise connectées MQTT

Un système de prises connéctées MQTT scalable et dynamique avec interface web et application androïd (webview),
basé sur un microcontroleur ESP8266 et un serveur WEB/MQTT.


# Sommaire
- [Mise en place et Installation](#mise-en-place-et-installation)
- [Matériel nécéssaire](#matériel-nécéssaire)
- [Fonctionnement](#fonctionnement)
- [Wishlist](#wishlist)



# Mise en place et Installation
* Le code  a été réalisé avec l'IDE arduino : il vous faudra suivre [ce guide](https://learn.adafruit.com/adafruit-feather-huzzah-esp8266/using-arduino-ide) pour pouvoir téléverser le code sur la carte.
* Suivre le schéma de cablage (un routage pour pcb est aussi dispobible)

* n.b. : pour avoir une mise à jour rapide de l'état enable/disable de la prise :
    changez la valeur "MQTT_KEEPALIVE" de 15 à 2 dans 
    ```cpp
    C:\Users\[user]\Documents\Arduino\libraries\PubSubClient\src
    ```
    ou
    ```cpp
    C:\Program Files\Arduino\libraries\PubSubClient\src
    ```
* Installation du serveur :




# Matériel nécéssaire

## Prises
- [Adafruit Feather HUZZAH with ESP8266](https://www.adafruit.com/product/2821)
- convertisseur [Myrra 48022](http://www.farnell.com/datasheets/2339616.pdf?_ga=2.58734052.899696324.1558094983-153532605.1558094983)
- Relais [V23105A5001A201](http://www.farnell.com/datasheets/1717878.pdf?_ga=2.154263920.978016925.1558335213-153532605.1558094983)
- Transistor [2N3904](https://www.onsemi.com/pub/Collateral/2N3903-D.pdf)
- Diode [1N4148](https://www.gotronic.fr/art-1n4148-7.htm)


## Serveur
- Raspberry pi

# Dépendances

## Prises
* [IDE arduino](https://www.arduino.cc/en/Main/Software) pour installer le code sur la carte
* Les librairies arduino utilisées sont :
    - FS.h : gestion de la mémoire de la carte
    - [WifiManager pour esp8266](https://github.com/tzapu/WiFiManager) : gestion du Wifi et de l'Access Point de règlage Wifi/MQTT
    - [ArduinoJson](https://github.com/bblanchon/ArduinoJson ) : La sauvegarde des réglages et les messages MQTT sont gérés en json
    - [PubSubClient](https://pubsubclient.knolleary.net/index.html) : Gestion de la connection MQTT




# Fonctionnement

## Prises
Au premier branchement, la prise se comporte en Access Point 
**ssid** : CONFIGURATION-PRISE **password** : CONF-ESP8266
On vous demandera alors :

* Un nom pour la prise
* un réseaux wifi ou se connectés
* les informations du serveur MQTT : adresse IP, port, utilisateur, mot de passe

Après paramétrage, la prise s'annonce sur un topic "signal" (@mac, utilisé comme ID MQTT, et nom de la prise). Le serveur va alors créer un topic "control/@mac" sur lequel la prise va écouté les ordres du serveur.

On peut changer l'état de la prise directement avec un des boutons de la prise (ONOFF_PIN). Un message MQTT serra envoyé au serveur.

Un bouton reset existe aussi (RES_PIN), pour relance la page de configuration si nécéssaire.

## Serveur MQTT

## Web
Un json est échangé entre le serveur et le client, contenant les prise, leur nom, leur activation et leur état.
Un websoket est aussi créer : le client web javascript écoutera alors les échanges MQTT en temps réel et tiendra la page à jours.
Les ordres de la page web serront envoyés en MQTT.




# Wishlist

## Prise
* amélioration de l'interface web de l'A.P. : [possible avec wifiManager](https://github.com/tzapu/WiFiManager#custom-parameters)

## MQTT

## Web